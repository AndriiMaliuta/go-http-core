# syntax=docker/dockerfile:1

FROM golang:1.18-rc-alpine
WORKDIR /app
COPY . ./
#RUN go mod download
#RUN go build -o /go-http-core
RUN go build -buildvcs=false && ls -lt
#COPY go-http-core ./
#EXPOSE 8082
#CMD [ "go-http-core" ]
CMD [ "ls -lta" ]